const actions = (storeState = {}, action) => {
  const modPin = {};

  if (action.type === 'MOD_PIN_STATE') {
    modPin[action.pin] = action.val;
  }

  function getState(state) {
    const newState = state;
    Object.entries(newState).forEach(([key, value]) => {
      newState[key] = value === 'true';
    });
    return newState;
  }
  function movePinToDisabled(pin, state) {
    const { crons } = state;
    if (crons.enabled.length === 0) {
      return state;
    }
    const newDisabled = crons.disabled;
    newDisabled[pin] = crons.enabled[pin];
    delete crons.enabled[pin];
    crons.disabled = newDisabled;

    return {
      ...state,
      crons,
    };
  }

  function movePinToEnabled(pin, state) {
    const { crons } = state;
    if (crons.disabled.length === 0) {
      return state;
    }
    const newEnabled = crons.enabled;
    newEnabled[pin] = crons.disabled[pin];
    delete crons.disabled[pin];
    crons.enabled = newEnabled;

    return {
      ...state,
      crons,
    };
  }

  function modPinData(pinNum, data) {
    const { enabled } = storeState.crons;
    const pin = enabled[pinNum];

    pin.name = data.name;
    pin.crons = data.crons;

    enabled[pinNum] = pin;

    return enabled;
  }

  switch (action.type) {
    case 'MOD_PIN_STATE':
      return {
        ...storeState,
        state: {
          ...storeState.state,
          ...modPin,
        },
      };
    case 'MOD_PIN_STATUS':
      if (action.enabled) {
        return movePinToDisabled(action.pin, storeState);
      }
      return movePinToEnabled(action.pin, storeState);
    case 'MOD_PIN_DATA':
      return {
        ...storeState,
        crons: {
          disabled: {
            ...storeState.crons.disabled,
          },
          enabled: {
            ...modPinData(action.pin, action.data),
          },
        },
      };
    case 'INIT_DATA':
      return {
        state: getState(action.state),
        crons: action.crons,
      };
    default:
      return storeState;
  }
};

export default actions;
