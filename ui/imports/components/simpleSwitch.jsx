import React from 'react';
import PropTypes from 'prop-types';

import styleSwitch from '../style/simpleSwitch.scss';

const SimpleSwitch = ({ checked, className, onClickHandler }) => (
  <label htmlFor="input" className={`${styleSwitch.switch} ${className}`}>
    <input type="checkbox" checked={!checked} readOnly onClick={() => onClickHandler(!checked)} />
    <span />
  </label>
);

SimpleSwitch.propTypes = {
  checked: PropTypes.bool.isRequired,
  className: PropTypes.string,
  onClickHandler: PropTypes.func.isRequired,
};

SimpleSwitch.defaultProps = {
  className: '',
};

export default SimpleSwitch;
