import React from 'react';
import { connect } from 'react-redux';

import Pin from './pin';

import styleFrame from '../style/frame.scss';

const getDisabled = (disabled, state, modPinState, modPinStatus) => {
  const pins = [];
  Object.entries(disabled).forEach(
    ([key, value]) => {
      pins.push(
        <li key={key}>
          <Pin
            number={key}
            name={value.name}
            pinState={state[key]}
            modPinState={modPinState}
            modPinStatus={modPinStatus}
          />
        </li>,
      );
    },
  );
  return pins;
};

const DisabledPins = ({ store, modPinState, modPinStatus }) => (
  <ul className={styleFrame.list}>
    {getDisabled(store.crons.disabled, store.state, modPinState, modPinStatus)}
  </ul>
);

const mapStateToProps = state => ({
  store: state,
});

const mapDispatchToProps = dispatch => ({
  modPinState: (pin, val) => {
    fetch(`/api/v1/pin/${pin}/${val ? 'high' : 'low'}`)
      .then(body => body.json())
      .then((data) => {
        if (data.status) {
          dispatch({
            type: 'MOD_PIN_STATE',
            pin,
            val,
          });
        } else {
          alert(data.error);
        }
      })
      .catch((error) => {
        alert(error);
      });
  },
  modPinStatus: (pin, enabled) => {
    fetch(`/api/v1/cron/enable/${pin}/`)
      .then(body => body.json())
      .then((data) => {
        if (data.status) {
          dispatch({
            type: 'MOD_PIN_STATUS',
            pin,
            enabled,
          });
        } else {
          alert(data.error);
        }
      })
      .catch((error) => {
        alert(error);
      });
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DisabledPins);
