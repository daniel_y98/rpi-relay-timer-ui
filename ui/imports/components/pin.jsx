import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import SimpleSwitch from './simpleSwitch';

import pinStyle from '../style/pin.scss';
import frameStyle from '../style/frame.scss';

const Pin = ({
  number, name, pinState, modPinState, modPinStatus, enabled,
}) => (
  <div className={`${frameStyle.flex} ${pinStyle.pin}`}>
    <div>
      <span className={pinStyle.number}>
        {number}
      </span>
      <span className={pinStyle.name}>
        {name}
      </span>
    </div>
    <div className={frameStyle.iconGroup}>
      { enabled
        ? (
          <Link to={`/edit/${number}`} className={frameStyle.btn}>
            <i className={`material-icons ${frameStyle.btn}`}>
edit
            </i>
          </Link>
        )
        : null
      }
      <button type="button" className={frameStyle.btn} onClick={() => modPinStatus(number, enabled)}>
        {enabled ? 'Disable' : 'Enable'}
      </button>
    </div>
    <SimpleSwitch
      checked={pinState}
      className={pinStyle.state}
      onClickHandler={checked => modPinState(number, checked)}
    />
  </div>
);

Pin.propTypes = {
  number: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  pinState: PropTypes.bool.isRequired,
  modPinState: PropTypes.func.isRequired,
  modPinStatus: PropTypes.func.isRequired,
  enabled: PropTypes.bool,
};

Pin.defaultProps = {
  enabled: false,
};

export default Pin;
