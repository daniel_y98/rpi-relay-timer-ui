import React from 'react';

import SimpleSwitch from './simpleSwitch';
import Input from './input';

import styleEdit from '../style/edit.scss';
import styleFrame from '../style/frame.scss';

const getCrons = (crons, actionChangeHandler, nameChangeHandler, cronDeleteHandler) => {
  const cronsList = [];
  crons.forEach(
    (cron, index) => {
      cronsList.push(
        <div className={styleEdit.cronContainer} key={`cron_${index}`}>
          <Input
            value={cron.name}
            name=""
            onChangeHandler={name => nameChangeHandler(cron.name, name)}
          />
          <SimpleSwitch
            checked={cron.action}
            onClickHandler={checked => actionChangeHandler(cron.name, checked)}
          />
          <button type="button" onClick={() => cronDeleteHandler(cron.name)}>
x
          </button>
        </div>,
      );
    },
  );

  return cronsList;
};

const CronsEdit = ({
  crons, actionChangeHandler, nameChangeHandler, addCronHandler, cronDeleteHandler,
}) => (
  <div className={styleFrame.content}>
    <h3>
      Crons
    </h3>
    <button type="button" onClick={addCronHandler}>
ADD
    </button>
    {getCrons(crons, actionChangeHandler, nameChangeHandler, cronDeleteHandler)}
  </div>
);

export default CronsEdit;
