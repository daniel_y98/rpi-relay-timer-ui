import React from 'react';
import { connect } from 'react-redux';

import Base from '../containers/base';
import SimpleSwitch from './simpleSwitch';
import Input from './input';
import CronsEdit from './cronsEdit';

import styleEdit from '../style/edit.scss';
import styleFrame from '../style/frame.scss';


class Edit extends React.Component {
  constructor(props) {
    super(props);

    const pin = props.match.params.id;
    this.pin = pin;

    this.state = {
      name: props.store.crons.enabled[pin].name,
      crons: props.store.crons.enabled[pin].crons,
    };
  }

  cronActionSwitchHandler(name, action) {
    this.setState(
      (prevState) => {
        const { crons } = prevState;

        fetch(`/api/v1/cron/edit/${this.pin}/${action ? 'high' : 'low'}/?cron=${encodeURIComponent(name)}`)
          .then(body => body.json())
          .then((data) => {
            if (!data.status) {
              alert(data.error);
            }
          })
          .catch((error) => {
            alert(error);
          });

        for (let i = 0; i < crons.length; i += 1) {
          const cron = crons[i];
          if (cron.name === name) {
            crons[i].action = action;
          }
        }

        return {
          ...prevState,
          crons,
        };
      },
    );
  }

  cronNameOnChangeHandler(name, newName) {
    this.setState(
      (prevState) => {
        const { crons } = prevState;

        for (let i = 0; i < crons.length; i += 1) {
          const cron = crons[i];
          if (cron.name === name) {
            crons[i].name = newName;
            break;
          }
        }

        return {
          ...prevState,
          crons,
        };
      },
    );
  }

  addCron() {
    this.setState(
      (prevState) => {
        const { crons } = prevState;

        for (let i = 0; i < crons.length; i += 1) {
          const cron = crons[i];
          if (cron.name === '') {
            return prevState;
          }
        }

        crons.push({ name: '', action: true });

        return {
          ...prevState,
          crons,
        };
      },
    );
  }

  cronDelete(cronName) {
    this.setState(
      (prevState) => {
        const { crons } = prevState;

        fetch(`/api/v1/cron/remove/${this.pin}/?cron=${encodeURIComponent(cronName)}`)
          .then(body => body.json())
          .then((data) => {
            if (!data.status) {
              alert(data.error);
            }
          })
          .catch((error) => {
            alert(error);
          });

        for (let i = 0; i < crons.length; i += 1) {
          const cron = crons[i];
          if (cron.name === cronName) {
            crons.splice(i, 1);
            break;
          }
        }

        return {
          ...prevState,
          crons,
        };
      },
    );
  }

  render() {
    const {
      match, store, modPinState, modPinData, history,
    } = this.props;
    const {
      crons,
      name,
    } = this.state;
    return (
      <Base>
        <div className={styleEdit.titleContainer}>
          <h2>
            <span className={styleEdit.greyTitle}>
      PIN
            </span>
            {' '}
            {match.params.id}
          </h2>
          <SimpleSwitch
            checked={store.state[match.params.id]}
            onClickHandler={checked => modPinState(match.params.id, checked)}
          />
        </div>
        <div className={styleFrame.content}>
          <Input
            value={name}
            name="Name"
            onChangeHandler={(value) => {
              if (value.length) {
                fetch(`/api/v1/cron/rename/${this.pin}/${value}`)
                  .then(body => body.json())
                  .then((data) => {
                    if (!data.status) {
                      alert(data.error);
                    }
                  })
                  .catch((error) => {
                    alert(error);
                  });
              }
              this.setState(
                prevState => ({
                  ...prevState,
                  name: value,
                }),
              );
            }
            }
          />
        </div>
        <CronsEdit
          crons={crons}
          actionChangeHandler={(cronName, action) => this.cronActionSwitchHandler(cronName, action)}
          nameChangeHandler={(cronName, newName) => this.cronNameOnChangeHandler(cronName, newName)}
          addCronHandler={() => this.addCron()}
          cronDeleteHandler={cronName => this.cronDelete(cronName)}
          key="crons"
        />
        <div className={styleFrame.content}>
          <button
            type="button"
            onClick={() => {
              modPinData(match.params.id, this.state);
              history.push('/');
            }}
          >
            Save
          </button>
        </div>
      </Base>
    );
  }
}

const mapStateToProps = state => ({
  store: state,
});

const mapDispatchToProps = dispatch => ({
  modPinState: (pin, val) => {
    fetch(`/api/v1/pin/${pin}/${val ? 'high' : 'low'}`)
      .then(body => body.json())
      .then((data) => {
        if (data.status) {
          dispatch({
            type: 'MOD_PIN_STATE',
            pin,
            val,
          });
        } else {
          alert(data.error);
        }
      })
      .catch((error) => {
        alert(error);
      });
  },
  modPinData: (pin, data) => {
    for (let i = 0; i < data.crons.length; i += 1) {
      fetch(`/api/v1/cron/add/${pin}/${data.crons[i].action ? 'high' : 'low'}/?cron=${encodeURIComponent(data.crons[i].name)}`)
        .then(body => body.json())
        .then((fetchData) => {
          if (!fetchData.status) {
            alert(fetchData.error);
          } else if (i >= data.crons.length - 1) {
            dispatch({
              type: 'MOD_PIN_DATA',
              pin,
              data,
            });
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Edit);
