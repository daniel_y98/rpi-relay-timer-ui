import React from 'react';

import Base from '../containers/base';

import EnabledPins from './enabledPins';
import DisabledPins from './disabledPins';

import styleFrame from '../style/frame.scss';

const Home = () => (
  <Base>
    <div className={styleFrame.content}>
      <h2 className={styleFrame.subTitle}>
Enabled
      </h2>
      <EnabledPins />
    </div>

    <div className={styleFrame.content}>
      <h2 className={styleFrame.subTitle}>
Disabled
      </h2>
      <DisabledPins />
    </div>
  </Base>
);

export default Home;
