import React from 'react';

import styleInput from '../style/input.scss';

const Input = ({ value, name, onChangeHandler }) => (
  <div className={styleInput.inputContainer}>
    <label htmlFor={name}>
      {name}
      <input type="text" onChange={event => onChangeHandler(event.target.value)} name={name} value={value} className={styleInput.input} />
    </label>
  </div>
);

export default Input;
