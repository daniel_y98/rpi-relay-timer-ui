import React from 'react';

import styleFrame from '../style/frame.scss';

const Base = ({ children }) => (
  <div className={`${styleFrame.container} ${styleFrame.widthFs}`}>
    <div className={styleFrame.titleContainer}>
      <h1 className={styleFrame.title}>
RPI Relay Timer
      </h1>
    </div>
    <div className={styleFrame.content}>
      {children}
    </div>
  </div>
);

export default Base;
