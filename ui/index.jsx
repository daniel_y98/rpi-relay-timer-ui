import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';

import Home from './imports/components/home';
import Edit from './imports/components/edit';

fetch('/data')
  .then(data => data.json())
  .then(data => store.dispatch({
    type: 'INIT_DATA',
    crons: data.crons,
    state: data.state,
  }));

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route
          exact
          path="/"
          component={Home}
        />
        <Route
          exact
          path="/edit/:id"
          component={Edit}
        />
      </Switch>
    </Router>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('app'));
