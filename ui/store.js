import { createStore } from 'redux';
import actions from './actions';

const store = createStore(actions, {
  state: {
  },
  crons: {
    enabled: {
    },
    disabled: {
    },
  },
});

export default store;
