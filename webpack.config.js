const path = require('path');

const BUILD_DIR = path.resolve(__dirname, 'static');
const APP_DIR = path.resolve(__dirname, 'ui');

const config = {
  entry: `${APP_DIR}/index.jsx`,
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        exclude: /node_modules/,
        include: APP_DIR,
        loader: ['babel-loader'],
      },
      {
        test: /\.scss?/,
        exclude: /node_modules/,
        include: APP_DIR,
        loader: 'style-loader!css-loader?modules=true&localIdentName=[name]__[local]___[hash:base64:5]!sass-loader',
      },
      {
        test: /\.css?/,
        exclude: /node_modules/,
        include: APP_DIR,
        loader: 'style-loader!css-loader',
      },
    ],
  },
  mode: 'development',
  resolve: {
    extensions: ['.js', '.jsx'],
  },
};

module.exports = config;
