from bottledaemon import daemon_run
from webapp import *

# The following lines will call the BottleDaemon script and launch a daemon in the background.
if __name__ == "__main__":
	daemon_run(host='0.0.0.0', port=80)
