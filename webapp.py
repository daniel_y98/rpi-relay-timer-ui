from bottle import static_file, route
import json
from settings import *
from app.api import *

@route('/')
def index_html():
	return static_file('index.html', root=STATIC)

@route('/edit/<pin>')
def index_html_edit_pin(pin):
	return static_file('index.html', root=STATIC)

@route('/static/<filename>')
def static(filename):
	return static_file(filename, root=STATIC)

@route('/data')
@route('/data/')
def get_data():
	state = {}
	crons = {}
	with open(STATE_PATH) as state_file:
		state = json.load(state_file)
	with open(CRONS_PATH) as crons_file:
		crons = json.load(crons_file)

	return {'state': state, 'crons': crons}
