from bottle import route, request
from settings import *
import subprocess


def run_command(command):
	error = ''
	message = ''
	status = False

	try:
		process = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		output, error = process.communicate()
		status = process.returncode == 0
		message = output
		error = error
	except Exception as e:
		error = e
		status = False

	return {'status': status, 'error': str(error), 'message': str(message)}


@route('/api/v1/pin/<number>/<output>')
@route('/api/v1/pin/<number>/<output>/')
def pin_output(number, output):
	if output not in ['high', 'low']:
		return {'status': False, 'error': 'Invalid output.'}
	if not number.isdigit():
		return {'status': False, 'error': 'Invalid pin.'}

	return run_command('{} pin {} {} -s {}'.format(
		RPI_TIMER_PATH, number, output, STATE_PATH))


@route('/api/v1/cron/enable/<number>')
@route('/api/v1/cron/enable/<number>/')
def pin_status_disable(number):
	if not number.isdigit():
		return {'status': False, 'error': 'Invalid pin.'}

	return run_command('{} cron enable --pin {} -c {}'.format(
		RPI_TIMER_PATH, number, CRONS_PATH))


@route('/api/v1/cron/disable/<number>')
@route('/api/v1/cron/disable/<number>/')
def pin_status_enable(number):
	if not number.isdigit():
		return {'status': False, 'error': 'Invalid pin.'}

	return run_command('{} cron disable --pin {} -c {}'.format(
		RPI_TIMER_PATH, number, CRONS_PATH))


@route('/api/v1/cron/rename/<number>/<name>')
@route('/api/v1/cron/rename/<number>/<name>/')
def cron_rename(number, name):
	if not number.isdigit():
		return {'status': False, 'error': 'Invalid pin.'}
	if len(name) == 0:
		return {'status': False, 'error': 'Invalid name.'}
	for sym in ESCAPE_SYMS:
		if sym in name:
			return {'status': False, 'error': 'Invalid name.'}

	return run_command('{} cron rename --pin {} --name "{}" -c {}'.format(
		RPI_TIMER_PATH, number, name, CRONS_PATH))


@route('/api/v1/cron/add/<number>/<output>')
@route('/api/v1/cron/add/<number>/<output>/')
def cron_add(number, output):
	cron = request.query['cron']
	if not number.isdigit():
		return {'status': False, 'error': 'Invalid pin.'}
	for sym in ESCAPE_SYMS:
		if sym in cron and sym != '/' and sym != '*':
			return {'status': False, 'error': 'Invalid cron.'}
	if output not in ['high', 'low']:
		return {'status': False, 'error': 'Invalid output.'}

	return run_command('{} cron add --pin {} --cron "{}" --output {} -c {}'.format(
		RPI_TIMER_PATH, number, cron, output, CRONS_PATH))


@route('/api/v1/cron/remove/<number>')
@route('/api/v1/cron/remove/<number>/')
def cron_remove(number):
	cron = request.query['cron']
	if not number.isdigit():
		return {'status': False, 'error': 'Invalid pin.'}
	if len(cron) == 0:
		return {'status': False, 'error': 'Invalid cron.'}
	for sym in ESCAPE_SYMS:
		if sym in cron and sym != '/' and sym != '*':
			return {'status': False, 'error': 'Invalid cron.'}

	return run_command('{} cron remove --pin {} --cron "{}" -c {}'.format(
		RPI_TIMER_PATH, number, cron, CRONS_PATH))

@route('/api/v1/cron/edit/<number>/<output>')
@route('/api/v1/cron/edit/<number>/<output>/')
def cron_remove(number, output):
	cron = request.query['cron']
	if not number.isdigit():
		return {'status': False, 'error': 'Invalid pin.'}
	if len(cron) == 0:
		return {'status': False, 'error': 'Invalid cron.'}
	for sym in ESCAPE_SYMS:
		if sym in cron and sym != '/' and sym != '*':
			return {'status': False, 'error': 'Invalid cron.'}

	return run_command('{} cron edit --pin {} --cron "{}" --output {} -c {}'.format(
		RPI_TIMER_PATH, number, cron, output, CRONS_PATH))
