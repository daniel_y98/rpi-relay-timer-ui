# RPI Timer UI
This is a front-end for https://gitlab.com/daniel_y98/rpi-relay-timer. It's not fancy but it works.

## Installation
Be sure to install the ``rpi-relay-timer`` before moving on to this.
Clone the repository into ``/opt``.

```bash
cd /opt
git clone https://gitlab.com/daniel_y98/rpi-relay-timer-ui.git
```

Install the node modules.

```bash
npm install
```

Then compile the bundle.

```bash
node_modules/webpack/bin/webpack.js -p
```

Install the python dependencies.

```bash
pip install -r requirements.txt
```

## Run the server

```bash
python /opt/rpi-relay-timer-ui/srv.py start
```

This will start a daemon on port 80.
