STATIC='/opt/rpi-relay-timer-ui/static'
RPI_TIMER_PATH='rpi_timer'
CRONS_PATH = '/var/lib/rpi_timer/crons.json'
STATE_PATH = '/var/lib/rpi_timer/state.json'
ESCAPE_SYMS = [
	'&',
	';',
	'\\',
	'/',
	'"',
	'\'',
	'-',
	'+',
	'%',
	'^',
	'*',
	'#',
	'!',
	'=',
	'~',
	'`',
	'(', ')',
	'$',
	'{',
	'}'
]
