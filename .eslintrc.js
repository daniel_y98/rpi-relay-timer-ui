module.exports = {
    "extends": "airbnb",
    "globals": {
      "document": true,
      "fetch": true,
      "alert": true
    },
    "rules": {
      "react/prop-types": 0,
      "no-alert": 0
    }
};
